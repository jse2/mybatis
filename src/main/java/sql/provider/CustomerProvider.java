package sql.provider;

import entity.Customer;
import org.apache.ibatis.builder.annotation.ProviderMethodResolver;
import org.apache.ibatis.jdbc.SQL;
import sql.util.Utils;

import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class CustomerProvider implements ProviderMethodResolver {

    private static final DateTimeFormatter format = DateTimeFormatter.ISO_DATE;

    private static String tableName = "customer";

    public static String insertCustomer(Customer customer) {

        return new SQL(){{
            INSERT_INTO(tableName);
            if (!Objects.isNull(customer.getLastName())) {
                VALUES("first_name", Utils.prepareValue(customer.getFirstName()));
            }
            if (!Objects.isNull(customer.getLastName())) {
                VALUES("last_name ", Utils.prepareValue(customer.getLastName()));
            }
            if (!Objects.isNull(customer.getBirthDate())) {
                VALUES("birth_date  ", Utils.prepareValue(customer.getBirthDate().format(format)));
            }

        }}.toString();
    }

    public static String updateCustomer(Customer customer) {
        return new SQL(){{
            UPDATE(tableName);
            if (!Objects.isNull(customer.getLastName())) {
                SET("first_name = #{firstName}");
            }
            if (!Objects.isNull(customer.getLastName())) {
                SET("last_name = #{lastName}");
            }
            if (!Objects.isNull(customer.getBirthDate())) {
                SET("birth_date = #{birthDate}");
            }
            WHERE("id = #{id}");
        }}.toString();
    }

}
