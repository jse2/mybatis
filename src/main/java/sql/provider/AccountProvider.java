package sql.provider;

import entity.Account;
import org.apache.ibatis.jdbc.SQL;
import sql.util.Utils;

import java.util.Objects;

public class AccountProvider {

    private static String tableName = "account";

    public static String insertAccount(Account account) {
        return new SQL(){{
            INSERT_INTO(tableName);
            if (!Objects.isNull(account.getCustomerId())) {
                VALUES("customer_id", Utils.prepareValue(String.valueOf(account.getCustomerId())));
            }
            if (!Objects.isNull(account.getAccountNumber())) {
                VALUES("account_number", Utils.prepareValue(account.getAccountNumber()));
            }
            if (!Objects.isNull(account.getBallance())) {
                VALUES("ballance", Utils.prepareValue(account.getBallance().toString()));
            }
        }}.toString();
    }

    public static String updateAccount(Account account) {
        return new SQL(){{
            UPDATE(tableName);
            if (!Objects.isNull(account.getCustomerId())) {
                SET("customer_id = #{customerId}");
            }
            if (!Objects.isNull(account.getBallance())) {
                SET("ballance = #{ballance}");
            }
            WHERE("account_number = #{accountNumber}");
        }}.toString();
    }

}
