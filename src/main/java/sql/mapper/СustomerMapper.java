package sql.mapper;

import entity.Customer;
import org.apache.ibatis.annotations.*;
import sql.provider.CustomerProvider;

import java.time.LocalDate;
import java.util.List;

public interface СustomerMapper {

    @Results(value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "birthDate", column = "birth_date", javaType = LocalDate.class)
    })
    @Select("SELECT * FROM customer")
    List<Customer> selectCustomers();

    @Results(value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "birthDate", column = "birth_date", javaType = LocalDate.class)
    })
    @Select("SELECT * FROM customer WHERE id = #{id}")
    Customer selectCustomer(long id);

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @InsertProvider(type = CustomerProvider.class, method = "insertCustomer")
    int createCustomer(Customer customer);

    @UpdateProvider(type = CustomerProvider.class, method = "updateCustomer")
    void updateCustomer(Customer customer);

    @Delete("delete from customer where id = #{id}")
    void deleteCustomer(Long id);

}
