package sql.mapper;

import entity.Account;
import org.apache.ibatis.annotations.*;
import sql.provider.AccountProvider;

import java.math.BigDecimal;
import java.util.List;

public interface AccountMapper {

    @Results(value = {
            @Result(property = "customerId", column = "customer_id", id = true),
            @Result(property = "accountNumber", column = "account_number"),
            @Result(property = "ballance", column = "ballance", javaType = BigDecimal.class)
    })
    @Select("SELECT * FROM account")
    List<Account> selectAccounts();

    @Results(value = {
            @Result(property = "customerId", column = "customer_id", id = true),
            @Result(property = "accountNumber", column = "account_number"),
            @Result(property = "ballance", column = "ballance", javaType = BigDecimal.class)
    })
    @Select("SELECT * FROM account WHERE account_number = #{accountNumber}")
    Account selectAccount(String accountNumber);

    @InsertProvider(type = AccountProvider.class, method = "insertAccount")
    int createAccount(Account account);

    @UpdateProvider(type = AccountProvider.class, method = "updateAccount")
    void updateAccount(Account account);

    @Delete("delete from account where account_number = #{accountNumber}")
    void deleteAccount(String accountNumber);

}
