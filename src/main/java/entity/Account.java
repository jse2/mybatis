package entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Getter
@Setter
@Accessors(chain = true)
public class Account {

    private long customerId;

    private String accountNumber;

    private BigDecimal ballance;

    @Override
    public String toString() {
        return String.format("customerId=%d accountNumber=%s ballance=%s", customerId, accountNumber, ballance.toString());
    }

}
