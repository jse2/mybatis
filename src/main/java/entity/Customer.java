package entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class Customer {

    private long id;

    private String firstName;

    private String lastName;

    private LocalDate birthDate;

    private List<Account> accounts;

    @Override
    public String toString() {
        return String.format("Id=%d firstName=%s lastName=%s birthDate=%s", id, firstName, lastName, birthDate.format(DateTimeFormatter.ISO_DATE));
    }
}
