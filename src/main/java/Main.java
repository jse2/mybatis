import entity.Account;
import entity.Customer;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import sql.mapper.AccountMapper;
import sql.mapper.СustomerMapper;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sqlSessionFactory =
                new SqlSessionFactoryBuilder().build(inputStream);
        try (SqlSession session = sqlSessionFactory.openSession()) {
            List<Customer> customer = testSelectCustomerWithAccount(session,
                    null,null,null,null,null);
            if (customer.size() != 16) {
                System.out.println("ERROR");
            }

            customer = testSelectCustomerWithAccount(session,
                    "Grigorii",null,null,null,null);
            if (customer.size() != 10) {
                System.out.println("ERROR");
            }

            customer = testSelectCustomerWithAccount(session,
                    "Test","Test",null,null,null);
            if (customer.size() != 6) {
                System.out.println("ERROR");
            }

            customer = testSelectCustomerWithAccount(session,
                    "Grigorii",null,null,"1",null);
            if (customer.size() != 1 && customer.get(0).getAccounts().size() != 1) {
                System.out.println("ERROR");
            }

            customer = testSelectCustomerWithAccount(session,
                    "Grigorii",null,null,"2",null);
            if (customer.size() != 0) {
                System.out.println("ERROR");
            }

            customer = testSelectCustomerWithAccount(session,
                    "Grigorii",null, LocalDate.of(2020, 12, 31),"1",null);
            if (customer.size() != 1) {
                System.out.println("ERROR");
            }

            customer = testSelectCustomerWithAccount(session,
                    "Grigorii",null, LocalDate.of(2020, 12, 30),"1",null);
            if (customer.size() != 0) {
                System.out.println("ERROR");
            }
        }
    }

    private static List<Customer> testSelectCustomerWithAccount(SqlSession session,
                                                                String firstName,
                                                                String lastName,
                                                                LocalDate birthDate,
                                                                String accountNumber,
                                                                BigDecimal ballance) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("firstName", firstName);
        params.put("lastName", lastName);
        params.put("birthDate", birthDate);
        params.put("accountNumber", accountNumber);
        params.put("ballance", ballance);
        return session.selectList("sql.mapper.CustomerAccountMapper.selectCustomerWithAccount", params);
    }

    private static void testAccount(SqlSession session) {
        AccountMapper mapper = session.getMapper(AccountMapper.class);
        /*createAccount(session);
        List<Account> accounts = mapper.selectAccounts();
        accounts.forEach(System.out::println);*/
        Account account = mapper.selectAccount("1");
        account.setCustomerId(2L);
        account.setBallance(new BigDecimal(20));
        updateAccount(session, account);
        System.out.println(mapper.selectAccount("1"));
        //deleteAccount(session, "1");
    }

    private static void createAccount(SqlSession session) {
        AccountMapper mapper = session.getMapper(AccountMapper.class);
        Account account = new Account().setCustomerId(1L).setAccountNumber("1").setBallance(new BigDecimal(100.19));
        mapper.createAccount(account);
        session.commit();
    }

    private static void updateAccount(SqlSession session, Account account) {
        AccountMapper mapper = session.getMapper(AccountMapper.class);
        mapper.updateAccount(account);
        session.commit();
    }

    private static void deleteAccount(SqlSession session, String accountNumber) {
        AccountMapper mapper = session.getMapper(AccountMapper.class);
        mapper.deleteAccount(accountNumber);
        session.commit();
    }

    private static void testCustomer(SqlSession session) {
        //long id = createCustomer(session);
        СustomerMapper mapper = session.getMapper(СustomerMapper.class);
        //List<Customer> customerList = mapper.selectCustomers();
        //customerList.forEach(System.out::println);
        //System.out.println(mapper.selectCustomer(id));
        //Customer customer = mapper.selectCustomer(19L);
        //customer.setFirstName("Elina");
        //updateCustomer(session, customer);
        //System.out.println(mapper.selectCustomer(19L));
        deleteCustomer(session, 19L);
    }

    private static long createCustomer(SqlSession session) {
        СustomerMapper mapper = session.getMapper(СustomerMapper.class);
        Customer customer = new Customer().setFirstName("Grigorii").setLastName("Mirsaitov").setBirthDate(LocalDate.now());
        mapper.createCustomer(customer);
        session.commit();
        return customer.getId();
    }

    private static void updateCustomer(SqlSession session, Customer customer) {
        СustomerMapper mapper = session.getMapper(СustomerMapper.class);
        mapper.updateCustomer(customer);
        session.commit();
    }

    private static void deleteCustomer(SqlSession session, Long id) {
        СustomerMapper mapper = session.getMapper(СustomerMapper.class);
        mapper.deleteCustomer(id);
        session.commit();
    }
}
